package netty

func sanitize(str string, escapeRunes ...rune) (string, error) {
	sanitizedStr := ""
	for _, char := range str {
		for _, escapeRune := range escapeRunes {
			if char == escapeRune {
				sanitizedStr += string(escapeRune)
			}
		}
		sanitizedStr += string(char)
	}
	return sanitizedStr, nil
}
