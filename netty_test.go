package netty

import (
	"fmt"
	"testing"
	"time"

	"github.com/pkg/errors"
	tools "gitlab.com/Pixdigit/goTestTools"
)

func TestEverything(t *testing.T) {
	errChan := make(chan error)

	nc1 := ConnectionConf{
		TCP,
		"localhost",
		49125,
		"localhost",
		49126,
	}
	c1, err := NewConnection(nc1)
	if err != nil {
		tools.WrapErr(err, "could not create first test connection", t)
	}

	nc2 := ConnectionConf{
		TCP,
		"localhost",
		49126,
		"localhost",
		49125,
	}
	c2, err := NewConnection(nc2)
	if err != nil {
		tools.WrapErr(err, "could not create second test connection", t)
	}

	c2.Accept(errChan)
	c1.Connect(errChan)

	type testStrStruct struct {
		Str string
	}

	tools.TestAgainstStrings(
		func(str string) error {
			fmt.Println("Testing  string: " + str)
			return c1.Send(testStrStruct{str})
		},
		func() (string, error) {
			timeout := false
			time.AfterFunc(1*time.Second, func() { timeout = true })
			var data testStrStruct
			var err error
			err = c2.Recv(&data);	if err != nil {return "", err}
			fmt.Println("Recieved string: " + data.Str)
			if timeout {
				return "", errors.New("network timeout")
			}
			return data.Str, err
		},
		"string test failed",
		t,
	)

	end := time.After(3 * time.Second)

	run := true
	for run {
		select {
		case err := <-errChan:
			if err != nil {
				tools.WrapErr(err, "error", t)
			} else {
				t.Log("some function pushed error with value nil")
			}
		case <-end:
			run = false
		default:
		}
	}

}
