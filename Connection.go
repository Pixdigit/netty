package netty

import (
	"bytes"
	"encoding/gob"
	"net"

	"github.com/pkg/errors"
)

type Connection struct {
	conf        ConnectionConf
	listener    net.Listener
	encoder     *gob.Encoder
	decoder     *gob.Decoder
	isConnected bool
}

func NewConnection(nc ConnectionConf) (Connection, error) {
	var err error
	newConn := Connection{}

	ok, err := nc.Valid()
	if !ok || err != nil {
		return Connection{}, errors.Wrap(err, "invalid network configuration")
	}

	newConn.conf = nc
	newConn.isConnected = false
	newConn.encoder = gob.NewEncoder(bytes.NewBuffer([]byte{}))
	newConn.decoder = gob.NewDecoder(bytes.NewBuffer([]byte{}))

	addr, err := newConn.conf.FullLocalAddress();	if err != nil {return Connection{}, errors.Wrap(err, "could not determine local address")}
	listener, err := net.Listen(newConn.conf.Protocol, addr);	if err != nil {return Connection{}, errors.Wrap(err, "unable to listen on "+addr)}
	newConn.listener = listener

	return newConn, nil
}

func (conn *Connection) Connect(errChan chan error) {
	if conn.isConnected {
		pushErr(errChan, errors.New("connection already established"))
	} else {
		remoteAddr, err := conn.conf.FullRemoteAddress()
		if err != nil {
			pushErr(errChan, errors.New("could not read remote addr"))
		} else {
			connection, err := net.Dial(conn.conf.Protocol, remoteAddr)
			if err != nil {
				pushErr(errChan, errors.New("unable to connect to "+remoteAddr))
			} else {
				conn.encoder = gob.NewEncoder(connection)
				conn.decoder = gob.NewDecoder(connection)
				conn.isConnected = true
			}
		}
	}
}

func (conn *Connection) Accept(errChan chan error) {
	go func() {
		for {
			//TODO: Check if connection comes from specified remote
			incomingConnection, err := conn.listener.Accept()
			if err != nil {
				pushErr(errChan, errors.Wrap(err, "failed accepting connection request"))
			} else if !conn.isConnected {
				conn.isConnected = true
				conn.encoder = gob.NewEncoder(incomingConnection)
				conn.decoder = gob.NewDecoder(incomingConnection)
			} else {
				pushErr(errChan, errors.New("connection already established"))
			}
		}
	}()
}

func (conn *Connection) Send(data interface{}) error {
	if !conn.isConnected {
		return errors.New("connection not yet established")
	}

	//Automatically writes to network
	err := conn.encoder.Encode(data);	if err != nil {return errors.Wrap(err, "unable to encode and data")}

	return nil
}

func (conn *Connection) Recv(dataStruct interface{}) error {
	err := conn.decoder.Decode(dataStruct);	if err != nil {return errors.Wrap(err, "error while recieving and decoding")}
	return nil
}

func (conn *Connection) Close() {
	conn.listener.Close()
	conn.isConnected = false
}
